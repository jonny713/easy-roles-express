const express = require('express')
const router = express.Router()
const auth = require('./auth')

router.get('/', function (req, res) {
  res.send({
    message: "easy-roles-express"
  })
})
router.post('/', function (req, res) {
  res.send({
    message: "easy-roles-express"
  })
})

router.use(auth)

module.exports = router
