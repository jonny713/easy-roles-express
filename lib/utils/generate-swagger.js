const { name, version } = require('./../../package.json');
const fs = require("fs");

const expressJSDocSwagger = require('express-jsdoc-swagger')
const options = {
  info: {
    version,
    title: name
  },
  security: {
    BasicAuth: {
      type: 'http',
      scheme: 'bearer',
    },
  },
  filesPattern: ['./../auth/**/*.js', './../core/**/*.js'],
  baseDir: __dirname,
  exposeSwaggerUI: false,
  exposeApiDocs: false,
}

// Подключаем документацию API
let listener = expressJSDocSwagger()(options)

listener.on('finish', (swaggerObject, processMethods) => {
  try {
    fs.writeFileSync('swagger.json', JSON.stringify(swaggerObject))
  } catch (e) {
    console.log(e);
  }
});
