const eError = require('@vl-utils/e-error')

const requiredMethods = {
  User: ['count', 'findOne', 'update', 'create'],
  Session: ['findOne', 'update', 'create'],
  Route: ['findOne'],
  Restore: ['findOne', 'update', 'create']
}

const db = {}

for (let entityName of Object.keys(requiredMethods)) {
  db[entityName] = {}
}

function updateDbMethods(entities) {
  if (typeof entities !== 'object') {
    throw new eError('Отсутствует объект entities')
  }

  for (let entityName of Object.keys(requiredMethods)) {
    // Проверяем, что в entities есть нужное поле и его тип - объект
    if (typeof entities[entityName] !== 'object') {
      throw new eError(`Отсутствует объект entities.${entityName}`)
    }

    const entity = entities[entityName]

    for (let methodName of requiredMethods[entityName]) {
      // Проверяем, что в entity есть нужный метод
      if (typeof entity[methodName] !== 'function') {
        throw new eError(`Отсутствует метод ${methodName} в entities.${entityName}`)
      }

      // Все проверки пройдены, добавляем метод
      db[entityName][methodName] = entity[methodName]
    }
  }
}

db.updateDbMethods = updateDbMethods


module.exports = db
