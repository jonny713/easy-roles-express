const CustomError = require('./../../helpers/CustomError')

const bcrypt = require("bcryptjs")
const jwt = require("jsonwebtoken")

const _config = require("../../config")


const express = require('express')
const router = express.Router()


const twoFA = require("../../middlewares/twoFA")

const extractIdFromToken = require("../../helpers/auth/extractIdFromToken")
const validator = require('../../helpers/validator')

const TravallinnoUser = require('../../travallinno-models/User')

const User = require('../../models/User')
const RestorePassword = require('./../../models/RestorePassword')
const Session = require('../../models/Session')

const Remainder = require('../../helpers/Remainder.js')
const expressValidator = require('./../../middlewares/express-validator.js');

const PHONE_TEMPLATE = /^(\+?7|8)\d{10}$/
const EMAIL_TEMPLATE = /^[^@]+@[^\.]+\.\w+$/
const PHONE_OR_EMAIL_TEMPLATE = new RegExp(`(${EMAIL_TEMPLATE.source}|${PHONE_TEMPLATE.source})`)
const INVALID_PHONE_OR_EMAIL_MSG = 'Телефон должен быть длиной 11 цифр. Почта должна быть в формате name@example.com'

const PASSWORD_TEMPLATE = /^\w{8,}$/
const INVALID_PASSWORD_MSG = 'Пароль не должен быть короче 8 символов'


/**
 *
 * @typedef {object} LoginRequest
 * @property {string} emailOrPhone.required - почта или телефон пользователя
 * @property {string} password.required - пароль пользователя
 */
// Проверить наличие всех необходимых полей
router.post('/login', expressValidator({
  emailOrPhone: {
    required: true,
    template: {
      regexp: PHONE_OR_EMAIL_TEMPLATE,
      message: INVALID_PHONE_OR_EMAIL_MSG
    }
  },
  password: {
    required: true,
    template: {
      regexp: PASSWORD_TEMPLATE,
      message: INVALID_PASSWORD_MSG
    }
  }
}))


// Получаем пользователя
router.post('/login', async (req, res, next) => {
  const { validated } = req
  let user
  try {
    if(PHONE_TEMPLATE.test(validated.emailOrPhone)) {
      user = await User.findOne({ phone: validated.emailOrPhone })
    } else {
      user = await User.findOne({ email: validated.emailOrPhone })
    }

    if (!user) {
      throw new CustomError({
        emailOrPhone: 'Пользователь не найден'
      })
    }
  } catch (e) {
    console.error(e)

    return res.status(404).send(e.parsedMessage)
  }

  try {
    if (await bcrypt.compare(validated.password, user.password)) {
      req.user = user

      return next()
    }

    throw new CustomError({
      password: 'Пароль неверен'
    })
  } catch (e) {
    console.error(e)

    return res.status(400).send(e.parsedMessage)
  }
})

// Двухфакторная аутентификация
router.post('/login', async (req, res, next) => {
  let { user } = req

  if (!user.twoFA) return next();

  if (!req.body.clientTwoFA) {
    const serverTwoFA = await new Validation({ user }).save();
    const { uuid } = serverTwoFA;
    console.log(`CREATED NEW KEY ${serverTwoFA.answer}`);

    return res.status(200).json({ uuid });
  }

  if (req.body.clientTwoFA) {
    const { uuid, answer: clientAnswer } = req.body.clientTwoFA;
    const serverTwoFA = await Validation.findOne({ uuid });
    const { answer: serverAnswer } = serverTwoFA;

    if (serverAnswer !== clientAnswer) {
      return res.status(404).json({ twoFA: "Код не совпадает" });
    }

    await Validation.deleteMany({ user: user._id });

    return next();
  }

  return res.status(500);
})

/**
 *
 * @typedef {object} LoginResponse
 * @property {string} token - строка токена
 * @property {IncompleteUser} user - данные пользователя
 */
/**
 *
 * @typedef {object} LoginResponseError
 * @property {string} message - Описание ошибки
 */
/**
 * POST /users/login
 * @summary Авторизация пользователя
 * @tags User api. Логин и регистрация
 * @param {LoginRequest} request.body.required
 * @return {LoginResponse} 200 - success response - application/json
 * @return {LoginResponseError} 400 - Некоторые параметры неверны - application/json
 * @return {LoginResponseError} 500 - Ошибка сервера - application/json
 */
router.post("/login", async (req, res) => {
  const { user } = req

  // ? Нужно ли обновлять пользователя при логине
  user.updatedAt = Date.now()
  await user.save()

  // Удаляем предыдущую сессию, если она есть
  await Session.findOneAndRemove({ userId: user.id })

  // Создаем новую сессию
  const session = new Session({
    userId: user.id
  })

  // Получаем токен
  const token = jwt.sign(
      { id: user.id, name: user.name, uuid: session.uuid },
      _config.jwt.secret,
      _config.jwt.options
  )

  await session.save()

  res.send({
    token,
    user: await user.getIncompleteUser()
  })
})


/**
 *
 * @typedef {object} SignupRequest
 * @property {string} emailOrPhone.required - телефон или почта пользователя - template:/(^[^@]+@[^\.]+\.\w+$|^7\d{10}$)/
 * @property {string} password.required - пароль пользователя - template:/^\w{8,}$/
 * @property {number} role.required - роль (может имеется ввиду тип - физическое/юридическое) пользователя
 */
//
router.post('/signup', expressValidator({
  emailOrPhone: {
    required: true,
    template: {
      regexp: PHONE_OR_EMAIL_TEMPLATE,
      message: INVALID_PHONE_OR_EMAIL_MSG
    }
  },
  password: {
    required: true,
    template: {
      regexp: PASSWORD_TEMPLATE,
      message: INVALID_PASSWORD_MSG
    }
  },
  role: {
    required: true,
    type: 'number'
  }
}))
 /**
  *
  * @typedef {object} SignupResponseError
  * @property {string} message - Описание ошибки
  */
 /**
  * POST /users/signup
  * @summary Регистрация пользователя
  * @tags User api. Логин и регистрация
  * @param {SignupRequest} request.body.required
  * @return {IncompleteUser} 200 - success response - application/json
  * @return {SignupResponseError} 400 - error response - application/json
  * @return {SignupResponseError} 500 - error response - application/json
  */
router.post('/signup', async (req, res, next) => {
  let {
    validated: registeringUser
  } = req

  // Проверяем, телефон или почта
  let identKey
  if(PHONE_TEMPLATE.test(registeringUser.emailOrPhone)) {
    registeringUser.phone = registeringUser.emailOrPhone
    identKey = 'phone'
  } else {
    registeringUser.email = registeringUser.emailOrPhone
    identKey = 'email'
  }

  delete registeringUser.emailOrPhone


  // Проверить, что в системе нет пользователя с указанной почтой или телефоном
  if (await User.findOne({ [identKey]: registeringUser[identKey] })) {
    return res.status(400).send({
        emailOrPhone: `${identKey == 'email' ? 'E-mail' : 'Телефон'} уже используется`
    })
  }

  // Создаем пользователя в траваллинно
  let travallinnoUserId
  try {
    let result = await TravallinnoUser.create(registeringUser)

    // Проверяем, что пользователь успешно создан в Траваллинно
    if (!result.id) {
      console.log(result)
      throw Error('Travallinno User not created')
    }

    travallinnoUserId = result.id
  } catch (e) {
    console.error(e)

    return res.status(500).send({
      error: "Возникла ошибка при регистрации",
      travallinno: e.parsedMessage && e.parsedMessage.error ? e.parsedMessage.error : undefined
    })
  }

  // Создаем пользователя в базе
  try {
    const createdUser = await new User({
      ...registeringUser,
      travalinoId: travallinnoUserId,
      password: await bcrypt.hash(registeringUser.password, 10)
    }).save()

    res.send(await createdUser.getIncompleteUser())
  } catch (e) {
    console.error(e)

    return res.status(500).send({
      error: "Возникла ошибка при регистрации"
    })
  }

  // Уведомить пользователя
  Remainder.signup({ [identKey]: registeringUser[identKey] })

})


/**
 *
 * @typedef {object} RestoreSendCodeRequest
 * @property {string} emailOrPhone.required - телефон или почта пользователя - template:/(^[^@]+@[^\.]+\.\w+$|^7\d{10}$)/
 */
//
router.post('/restore/send-code', expressValidator({
  emailOrPhone: {
    required: true,
    template: {
      regexp: PHONE_OR_EMAIL_TEMPLATE,
      message: INVALID_PHONE_OR_EMAIL_MSG
    }
  }
}))

/**
 * POST /users/restore/send-code
 * @summary Отправка кода на восстановление забытого пароля пользователя
 * @tags User api. Логин и регистрация
 * @param {RestoreSendCodeRequest} request.body.required
 * @return {empty} 200 - success response - application/json
 * @return {object} 400 - error response - application/json
 * @return {object} 500 - error response - application/json
 */
router.post('/restore/send-code', async (req, res, next) => {
  let {
    validated
  } = req

  // Проверяем, телефон или почта
  let identKey
  if(PHONE_TEMPLATE.test(validated.emailOrPhone)) {
    validated.phone = validated.emailOrPhone
    identKey = 'phone'
  } else {
    validated.email = validated.emailOrPhone
    identKey = 'email'
  }

  delete validated.emailOrPhone


  // Проверить, что в системе есть пользователь с указанной почтой или телефоном
  const user = await User.findOne({ [identKey]: validated[identKey] })

  if (!user) {
    return res.status(400).send({
        emailOrPhone: `Пользователь с таким ${identKey == 'email' ? 'e-mail' : 'телефоном'} не найден`
    })
  }

  // Удаляем лишние коды пользователя
  await RestorePassword.deleteMany({ userId: user._id})

  // Формируем код
  let restorePassword = new RestorePassword({
    userId: user._id
  })

  await restorePassword.save()

  // Отправляем код на указанный способ связи пользователя
  Remainder.restore({
    [identKey]: validated[identKey],
    // email: 'v.gorshkov@exist-techno.ru',
    // phone: '79629746858',
    code: restorePassword.code
  })

  res.send()
})

/**
 *
 * @typedef {object} RestorePasswordRequest
 * @property {string} emailOrPhone.required - телефон или почта пользователя - template:/(^[^@]+@[^\.]+\.\w+$|^7\d{10}$)/
 * @property {string} code.required - пароль пользователя - template:/^\d{6}$/
 * @property {string} password.required - пароль пользователя - template:/^\w{8,}$/
 */
//
router.post('/restore/password', expressValidator({
  emailOrPhone: {
    required: true,
    template: {
      regexp: PHONE_OR_EMAIL_TEMPLATE,
      message: INVALID_PHONE_OR_EMAIL_MSG
    }
  },
  code: {
    required: true,
    template: /^\d{6}$/
  },
  password: {
    required: true,
    template: {
      regexp: PASSWORD_TEMPLATE,
      message: INVALID_PASSWORD_MSG
    }
  },
}))

/**
 * POST /users/restore/password
 * @summary Восстановление забытого пароля пользователя
 * @tags User api. Логин и регистрация
 * @param {RestorePasswordRequest} request.body.required
 * @return {empty} 200 - success response - application/json
 * @return {object} 400 - error response - application/json
 * @return {object} 500 - error response - application/json
 */
router.post('/restore/password', async (req, res, next) => {
  let {
    validated
  } = req

  // Проверяем, телефон или почта
  let identKey
  if(PHONE_TEMPLATE.test(validated.emailOrPhone)) {
    validated.phone = validated.emailOrPhone
    identKey = 'phone'
  } else {
    validated.email = validated.emailOrPhone
    identKey = 'email'
  }

  delete validated.emailOrPhone


  // Проверить, что в системе есть пользователь с указанной почтой или телефоном
  const user = await User.findOne({ [identKey]: validated[identKey] })

  if (!user) {
    return res.status(400).send({
        emailOrPhone: `Пользователь с таким ${identKey == 'email' ? 'e-mail' : 'телефоном'} не найден`
    })
  }

  // Проверить, что пользователь делал запрос на изменение пароля и код совпадает
  const restorePassword = await RestorePassword.findOne({
    userId: user._id,
    code: validated.code
  })

  if (!restorePassword) {
    return res.status(400).send({
        code: `Не соответствующий код`
    })
  }

  // Удаляем код
  await restorePassword.remove()

  // Изменяем пароль пользователя
  user.password = await bcrypt.hash(validated.password, 10)
  await user.save()

  res.send()


  // Отправляем сообщение о смене пароля
  Remainder.restoreComplite({ [identKey]: validated[identKey] })
})


router.use(async (req, res, next) => {
  const { authorization } = req.headers
  if (!authorization) {
    return res.status(403).send({ code: 'not auth' })
  }

  try {
    const { id, uuid } = jwt.verify(authorization.split(" ")[1], "secret")

    const session = await Session.findOne({
      userId: id,
      uuid
    })

    if (!session) {
      throw Error('invalid token')
    }

    const user = await User.findById(id)

    if (!user) {
      throw Error('user not found')
    }

    req.user = user
    req.session = session

    next()
  } catch (e) {
    console.log(e)

    return res.status(403).send({ error: 'Вы не авторизованы' })
  }

})

/**
 *
 * @typedef {object} Error400
 * @property {string} message - Описание ошибки
 * @property {string} key - Поле, с которым возникла проблема
 */
/**
 *
 * @typedef {object} Error500
 * @property {string} message - Описание ошибки
 */
/**
 * GET /users/check-token
 * @summary Проверка токена
 * @tags User api. Логин и регистрация
 * @param {string} authorization.header.required
 * @return {IncompleteUser} 200 - success response - application/json
 * @return {Error500} 400 - Параметры неверны - application/json
 * @return {Error500} 500 - Ошибка сервера - application/json
 */
router.get('/check-token', async (req, res, next) => {
  let { user } = req
  res.json(await user.getIncompleteUser())
})

/**
 * POST /users/logout
 * @summary Выход из сессии пользователя
 * @tags User api. Логин и регистрация
 * @param {string} authorization.header.required
 * @return {User} 200 - success response - application/json
 * @return {SignupResponseError} 400 - Параметры неверны - application/json
 * @return {SignupResponseError} 500 - Ошибка сервера - application/json
 */
router.post('/logout', async (req, res) => {
  let { session } = req

  try {
    await Session.findByIdAndRemove(session.id)

    res.send({ message: 'success' })
  } catch (e) {
    console.log(e)

    return res.status(500).send({ error: 'Возникла ошибка сервера' })

  }
})

module.exports = router
