const PHONE_TEMPLATE = /^(\+?7|8)\d{10}$/
const EMAIL_TEMPLATE = /^[^@]+@[^\.]+\.\w+$/
const PHONE_OR_EMAIL_TEMPLATE = new RegExp(`(${EMAIL_TEMPLATE.source}|${PHONE_TEMPLATE.source})`)
const INVALID_PHONE_MSG = 'Телефон должен быть длиной 11 цифр'
const INVALID_EMAIL_MSG = 'Почта должна быть в формате name@example.com'
const INVALID_PHONE_OR_EMAIL_MSG = 'Телефон должен быть длиной 11 цифр. Почта должна быть в формате name@example.com'

const PASSWORD_TEMPLATE = /^\w{8,}$/
const INVALID_PASSWORD_MSG = 'Пароль не должен быть короче 8 символов'

module.exports = {
  PHONE_TEMPLATE,
  EMAIL_TEMPLATE,
  PHONE_OR_EMAIL_TEMPLATE,
  INVALID_PHONE_MSG,
  INVALID_EMAIL_MSG,
  INVALID_PHONE_OR_EMAIL_MSG,
  PASSWORD_TEMPLATE,
  INVALID_PASSWORD_MSG
}
