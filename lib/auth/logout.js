const expressValidator = require('@vl-utils/validator')

const express = require('express')
const router = express.Router()

// const {
//   remainder
// } = require('./../utils')

const { markRouteSecure } = require('./../core/Role')
const User = require('./../core/User')
const Session = require('./../core/Session')

/**
 * POST /logout
 * @summary Завершение сессии пользователя
 * @tags User api. Логин и регистрация
 * @return {SuccessResponse} 200 - success response - application/json
 * @return {ErrorResponse} 403 - Нет доступа - application/json
 * @return {ErrorResponse} 500 - Ошибка сервера - application/json
 */
router.post('/logout', markRouteSecure(), async (req, res, next) => {
  const { user } = req

  // Стартуем сессию, получаем токен
  await Session.stopSession(user)

  res.send({ success: true })
})


module.exports = router
