const expressValidator = require('@vl-utils/validator')

const express = require('express')
const router = express.Router()

const {
  EMAIL_TEMPLATE, INVALID_EMAIL_MSG,
  PASSWORD_TEMPLATE, INVALID_PASSWORD_MSG
} = require('./../templates')

const User = require('./../core/User')
const notifier = require('./../core/Notifier')

/**
 *
 * @typedef {object} SignupRequest
 * @property {string} email.required - почта пользователя - template:/^[^@]+@[^\.]+\.\w+$/
 * @property {string} password.required - пароль пользователя - template:/^\w{8,}$/
 */
router.post('/signup', expressValidator({
  email: {
    required: true,
    template: {
      regexp: EMAIL_TEMPLATE,
      message: INVALID_EMAIL_MSG
    }
  },
  password: {
    required: true,
    template: {
      regexp: PASSWORD_TEMPLATE,
      message: INVALID_PASSWORD_MSG
    }
  }
}))

/**
 *
 * @typedef {object} SignupResponseError
 * @property {string} message - Описание ошибки
 */
/**
 * POST /signup
 * @summary Регистрация пользователя
 * @tags User api. Логин и регистрация
 * @param {SignupRequest} request.body.required
 * @return {IncompleteUser} 200 - success response - application/json
 * @return {ErrorResponse} 400 - error response - application/json
 * @return {ErrorResponse} 500 - error response - application/json
 */
router.post('/signup', async (req, res, next) => {
  let {
    validated: { email, password }
  } = req


  // Проверить, что в системе нет пользователя с указанной почтой или телефоном
  if (await User.findOne({ email })) {
    return res.status(400).send({
        email: `E-mail уже используется`
    })
  }


  // Создаем пользователя в базе
  try {
    const createdUser = await User.signup({ email, password })

    res.send({
      user: createdUser.secure()
    })
  } catch (e) {
    console.error(e)

    return res.status(500).send({
      error: "Возникла ошибка при регистрации"
    })
  }

  // Уведомить пользователя
  notifier.signup({ email })

})


module.exports = router
