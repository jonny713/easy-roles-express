const expressValidator = require('@vl-utils/validator')

const express = require('express')
const router = express.Router()

const {  markRouteSecure } = require('./../core/Role')


const {
  remainder
} = require('./../utils')

const User = require('./../core/User')


/**
 *
 * @typedef {object} SignupResponseError
 * @property {string} message - Описание ошибки
 */
/**
 * GET /user
 * @summary Получение пользователя
 * @tags User api. Пользователь
 * @param {SignupRequest} request.body.required
 * @return {IncompleteUser} 200 - success response - application/json
 * @return {ErrorResponse} 404 - error response - application/json
 * @return {ErrorResponse} 500 - error response - application/json
 */
router.get('/user', markRouteSecure(), async (req, res, next) => {
  // let {
  //   validated: { email }
  // } = req

  let user = req.user

  if (!user) {
    return res.status(404).send({
      error: "Пользователь не найден"
    })
  }

  // Создаем пользователя в базе
  res.send({
    user: await user.secure()
  })
})


module.exports = router
