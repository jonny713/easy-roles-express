const expressValidator = require('@vl-utils/validator')
const eError = require('@vl-utils/e-error')

const express = require('express')
const router = express.Router()

const {
  remainder
} = require('./../utils')

const {
  EMAIL_TEMPLATE, INVALID_EMAIL_MSG,
  PASSWORD_TEMPLATE, INVALID_PASSWORD_MSG
} = require('./../templates')

const User = require('./../core/User')
const Session = require('./../core/Session')


/**
 *
 * @typedef {object} LoginRequest
 * @property {string} email.required - почта пользователя
 * @property {string} password.required - пароль пользователя
 */
router.post('/login', expressValidator({
  email: {
    required: true,
    template: {
      regexp: EMAIL_TEMPLATE,
      message: INVALID_EMAIL_MSG
    }
  },
  password: {
    required: true,
    template: {
      regexp: PASSWORD_TEMPLATE,
      message: INVALID_PASSWORD_MSG
    }
  }
}))


// Получаем пользователя
router.post('/login', async (req, res, next) => {
  const {
    validated: { email, password }
  } = req

  let user
  try {
    user = await User.findOne({ email })


    if (!user) {
      throw new eError({
        email: 'Пользователь не найден'
      })
    }
  } catch (e) {
    console.error(e)

    return res.status(404).send(e.parsedMessage)
  }

  try {
    if (await user.comparePassword(password)) {
      req.user = user

      return next()
    }

    throw new eError({
      password: 'Пароль неверен'
    })
  } catch (e) {
    console.error(e)

    return res.status(400).send(e.parsedMessage)
  }
})

/**
 * @typedef {object} LoginResponse
 * @property {string} token - строка токена
 * @property {IncompleteUser} user - данные пользователя
 */

/**
 * POST /login
 * @summary Авторизация пользователя
 * @tags User api. Логин и регистрация
 * @param {LoginRequest} request.body.required
 * @return {LoginResponse} 200 - success response - application/json
 * @return {ErrorResponse} 400 - Некоторые параметры неверны - application/json
 * @return {ErrorResponse} 500 - Ошибка сервера - application/json
 */
router.post("/login", async (req, res) => {
  const { user } = req

  // Стартуем сессию, получаем токен
  const token = await Session.startSession(user)

  res.send({
    token,
    user: await user.secure()
  })
})


module.exports = router
