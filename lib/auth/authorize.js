const expressValidator = require('@vl-utils/validator')
// const CustomError = require('./../utils/CustomError.js')

const express = require('express')
const router = express.Router()

const {
  remainder
} = require('./../utils')

const {
  EMAIL_TEMPLATE, INVALID_EMAIL_MSG,
  PASSWORD_TEMPLATE, INVALID_PASSWORD_MSG
} = require('./../templates')

const Route = require('./../core/Route')
const Session = require('./../core/Session')
const User = require('./../core/User')

// Попытка получить пользователя по токену и проверка доступности запрошенного маршрута
router.use(async (req, res, next) => {
  const { authorization: token } = req.headers

  try {
    let { userId } = await Session.findOne({ token })
    req.user = await User.findOne({ id: userId })

  } catch (e) {
    // console.log('Не найден пользователь по токену');
  }

  return next()
})

module.exports = router
