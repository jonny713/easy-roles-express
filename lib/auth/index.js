const express = require('express')
const router = express.Router()

const authorize = require('./authorize')
const login = require('./login')
const logout = require('./logout')
const signup = require('./signup')
const restore = require('./restore')
const user = require('./user')

/**
 * @typedef {object} ErrorResponse
 * @property {string} message - Описание ошибки
 */

/**
 * @typedef {object} SuccessResponse
 * @property {boolean} success - true
 */
router.use(authorize)
router.use(login)
router.use(logout)
router.use(signup)
router.use(restore)
router.use(user)

module.exports = router
