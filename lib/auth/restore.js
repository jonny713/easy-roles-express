const expressValidator = require('@vl-utils/validator')

const express = require('express')
const router = express.Router()

const {
  EMAIL_TEMPLATE, INVALID_EMAIL_MSG,
  PASSWORD_TEMPLATE, INVALID_PASSWORD_MSG
} = require('./../templates')

const User = require('./../core/User')
const Restore = require('./../core/Restore')
const notifier = require('./../core/Notifier')

/**
 *
 * @typedef {object} RestoreSendCodeRequest
 * @property {string} email.required - почта пользователя
 */
router.post('/restore/send-code', expressValidator({
  email: {
    required: true,
    template: {
      regexp: EMAIL_TEMPLATE,
      message: INVALID_EMAIL_MSG
    }
  }
}))

/**
 * POST /restore/send-code
 * @summary Отправка кода на восстановление забытого пароля пользователя
 * @tags User api. Логин и регистрация
 * @param {RestoreSendCodeRequest} request.body.required
 * @return {SuccessResponse} 200 - success response - application/json
 * @return {ErrorResponse} 400 - error response - application/json
 * @return {ErrorResponse} 500 - error response - application/json
 */
router.post('/restore/send-code', async (req, res, next) => {
  let {
    validated
  } = req

  // Проверить, что в системе есть пользователь с указанной почтой
  const user = await User.findOne({ email: validated.email })

  if (!user) {
    return res.status(400).send({
        email: `Пользователь с такой почтой не найден`
    })
  }

  // Формируем код
  let code = await Restore.createCode(user)

  // Отправляем код на почту
  notifier.restore({
    email: user.email,
    code,
  })

  res.send({ success: true })
})

/**
 *
 * @typedef {object} RestorePasswordRequest
 * @property {string} email.required - телефон или почта пользователя
 * @property {string} code.required - пароль пользователя - template:/^\w{6}$/
 * @property {string} password.required - пароль пользователя - template:/^\w{8,}$/
 */
router.post('/restore/password', expressValidator({
  email: {
    required: true,
    template: {
      regexp: EMAIL_TEMPLATE,
      message: INVALID_EMAIL_MSG
    }
  },
  code: {
    required: true,
    template: /^\w{6}$/
  },
  password: {
    required: true,
    template: {
      regexp: PASSWORD_TEMPLATE,
      message: INVALID_PASSWORD_MSG
    }
  },
}))

/**
 * POST /restore/password
 * @summary Восстановление забытого пароля пользователя
 * @tags User api. Логин и регистрация
 * @param {RestorePasswordRequest} request.body.required
 * @return {SuccessResponse} 200 - success response - application/json
 * @return {ErrorResponse} 400 - error response - application/json
 * @return {ErrorResponse} 500 - error response - application/json
 */
router.post('/restore/password', async (req, res, next) => {
  let {
    validated
  } = req

  // Проверить, что в системе есть пользователь с указанной почтой или телефоном
  const user = await User.findOne({ email: validated.email })

  if (!user) {
    return res.status(400).send({
        email: `Пользователь с такой почтой не найден`
    })
  }

  // Проверить, что пользователь делал запрос на изменение пароля и код совпадает
  const restore = await Restore.findOne({
    userId: user.id,
    code: validated.code
  })

  if (!restore) {
    return res.status(400).send({
        code: `Не соответствующий код`
    })
  }

  // Удаляем код
  await Restore.deleteMany(user)

  // Изменяем пароль пользователя
  await User.changePass({ id: user.id, password: validated.password })

  // Отправляем сообщение о смене пароля
  notifier.restoreComplite({ email: user.email })

  res.send({ success: true })
})

module.exports = router
