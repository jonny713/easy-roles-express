/**
 * Отправка уведомлений пользователю
 */



class Remainder {
  addTransport({ transporter }) {
    this.transporter = transporter
  }

  send({ email, subject, text }) {
    if (!this.transporter) {
      return
    }
    this.transporter.send({
        text,
        // from: `Evil shrimps <info@evil-shrimps.ru>`,
        to: email,
        subject,
    }, (err, message) => console.log(err || message))
  }

  signup({ email } = {}) {
    this.send({
      email,
      subject: "Успешная регистрация",
      text: 'Вы были успешно зарегистрированы',
    })
  }

  restoreComplite({ email } = {}) {
    this.send({
      email,
      subject: "Успешная смена пароля",
      text: 'Ваш пароль на сайте был успешно изменен',
    })
  }

  restore({ email, code } = {}) {
    this.send({
      email,
      subject: "Восстановление пароля",
      text: `Код восстановление пароля: ${code}`,
    })
  }
}

let remainder
module.exports = new Remainder()
