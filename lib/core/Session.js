const { v4: uuid } = require('uuid');

const db = require('./../db-methods')

class Session {
  static async findOne(search = {}) {
    const session = await db.Session.findOne({ ...search, enabled: true })
    // Если не найден, возрващаем null
    if (!session) {
      return null
    }

    // Проверяем expire токена
    if (session.expireAt < Date.now()) {
      return null
    }

    // Все нормально, возвращаем сессию
    return session
  }

  static async startSession(user) {
    // Останавливаем прочие сессии, если есть
    await Session.stopSession(user)

    // Создаем экземпляр сессии
    let session = new Session(user)

    // Сохраняем новый токен для пользователя
    await db.Session.create(session)

    return session.token
  }

  static async stopSession(user) {
    await db.Session.update({ userId: user.id }, { enabled: false })
  }

  constructor(user) {
    this.userId = user.id
    this.enabled = true
    this.createdAt = new Date()
    this.expireAt = new Date(Date.now() + 1e3 * 60 * 60)
    this.token = uuid()
  }
}

module.exports = Session
