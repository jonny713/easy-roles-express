const argon2 = require('argon2');
const db = require('./../db-methods')

class User {
  static async findOne(search = {}) {
    const dbData = await db.User.findOne(search)

    if (dbData) {
      return new User(dbData)
    }
  }
  static async signup(user = {}) {
    user.password = await argon2.hash(user.password);
    let count = await db.User.count()

    const dbData = await db.User.create({
      id: count + 1,
      ...user,
      role: 1,
    })

    return new User(dbData)
  }
  static async changePass({ id, password } = {}) {
    password = await argon2.hash(password);

    const dbData = await db.User.update({ id }, { password })

    return new User(dbData)
  }

  id
  email
  password
  name
  role
  constructor(user = {}) {
    this.id = user.id
    this.email = user.email
    this.password = user.password
    this.name = user.name
    this.role = user.role
  }

  // Сверяет пароль
  async comparePassword(password) {
    return await argon2.verify(this.password, password)
  }

  /**
   * @summary Безопасную версию объекта пользователя
   * @typedef {object} IncompleteUser
   * @property {number} id - идентификатор
   * @property {string} email - почта
   * @property {string} name - имя
   * @property {number} role - идентификатор роли
   */
  async secure() {
    const secureUser = {
      id: this.id,
      email: this.email,
      name: this.name,
      role: this.role
    }
    return secureUser
  }
}

module.exports = User
