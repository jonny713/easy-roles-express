
const db = require('./../db-methods')

module.exports.markRouteSecure = (roles = [1]) => {
  return (req, res, next) => {
    if (roles.includes(req.user?.role)) {
      return next()
    }

    return res.status(403).send({ code: 'not auth' })
  }
}

class Role {
  static roles = {}
  static createRole() {

  }
}

module.exports.Role = Role
