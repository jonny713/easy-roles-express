const { v4: uuid } = require('uuid');

const db = require('./../db-methods')

class Restore {
  static async findOne(search = {}) {
    const restore = await db.Restore.findOne({ ...search, enabled: true })
    // Если не найден, возрващаем null
    if (!restore) {
      return null
    }

    // Проверяем expire токена
    if (restore.expireAt < Date.now()) {
      return null
    }

    // Все нормально, возвращаем сессию
    return restore
  }

  static async createCode(user) {
    // Удаляем прочие коды, если есть
    await Restore.deleteMany(user)

    // Создаем экземпляр с кодом восстановления
    let restore = new Restore(user)

    // Сохраняем новый токен для пользователя
    await db.Restore.create(restore)

    return restore.code
  }

  static async deleteMany(user) {
    await db.Restore.update({ userId: user.id }, { enabled: false })
  }

  constructor(user) {
    this.userId = user.id
    this.enabled = true
    this.createdAt = new Date()
    this.expireAt = new Date(Date.now() + 1e3 * 60 * 5)
    this.code = Math.random().toString(36).slice(2, 2 + Math.max(1, Math.min(6, 10)) );
  }
}

module.exports = Restore
