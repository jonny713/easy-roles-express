
const db = require('./../db-methods')

class Route {
  static async check(path, user) {
    const route = await db.Route.findOne(path)

    // Если маршрут не найден, значит он публичный
    if (!route) {
      return true
    }

    // Если маршрут найден, проверяем доступность конкретному пользователю
    if (route.usersWithPermission.includes[user.id]) {
      return true
    }

    // Проверяем доступность роли пользователя
    if (route.rolesWithPermission.includes[user.role?.id]) {
      return true
    }

    // Доступ к маршруту отсутствует
    return false
  }
}

module.exports = Route
