
const { updateDbMethods } = require('./lib/db-methods')
const router = require('./lib/index')
const { Role, markRouteSecure } = require('./lib/core/Role.js')
const notifier = require('./lib/core/Notifier.js')

try {
  const swagger = require('./swagger.json')
  
  delete swagger.info.title
  delete swagger.info.version

  module.exports.swagger = swagger
} catch (e) {}

module.exports.secure = () => {
  return markRouteSecure()
}

module.exports.authRouter = (dbMethods, notifierTransport = {}) => {
  // dbMethods - объект с методами-обработчиками для операций с пользователями
  // Необходимо проверить наличие всех методов для успешной работы
  // список необходимых методов хранится в /lib/db-methods

  updateDbMethods(dbMethods)

  notifier.addTransport(notifierTransport)

  return router
}
