const express = require('express')

const { authRouter, secure } = require('./../index.js')

const EMAIL = 'mail1@ya.ru'
const PASSWORD = '12345678'

const collector = { user: {}, session: {}, restore: {}, notifies: [(...args) => console.info(...args)] }
const dbMethods = {

  User: {
    count: () => {
      return Object.values(collector.user).length
    },
    findOne: (search = {}) => {
      if (search?.email) {
        return collector.user[search?.email]
      }
      return Object.values(collector.user).find(s => s.id === search.id)
    },
    update: (search = {}, data) => {
      if (collector.user[search?.email]) {
        collector.user[search?.email].password =
          data.password ||
          collector.user[search?.email].password
      }
      return collector.user[search?.email]
    },
    create: (data) => {
      let id = Object.values(collector.user).length
      collector.user[data.email] = {
        id,
        email: data.email,
        password: data.password,
        role: 1,
      }
      return collector.user[data.email]
    },
  },
  Session: {
    findOne: (search = {}) => {
      let session
      if (search?.userId) {
        session = collector.session[search?.userId]
      }
      session = Object.values(collector.session).find(s => s.token === search.token)
      return session.enabled ? session : null
    },
    update: (search = {}, data) => {
      if (collector.session[search?.userId]) {
        collector.session[search?.userId] = {
          ...collector.session[search?.userId],
          ...data,
          userId: search?.userId,
        }
      }
      return collector.session[search?.userId]
    },
    create: (data) => {
      let id = Object.values(collector.session).length
      collector.session[data.userId] = {
        id,
        ...data,
      }
      return collector.session[data.userId]
    },
  },
  Route: {
    findOne: (search = {}) => {
      if (search?.userId == 6) {
        return {
          id: 99,
          path: '/secure',
          method: 'GET',
          usersWithPermission: [6],
          rolesWithPermission: [77],
        }
      }
    },
  },
  Restore: {
    findOne: (search = {}) => {
      let session
      if (search?.userId) {
        restore = collector.restore[search?.userId]
      }
      restore = Object.values(collector.restore).find(s => s.code === search.code)
      return restore.enabled ? restore : null
    },
    update: (search = {}, data) => {
      if (collector.restore[search?.userId]) {
        collector.restore[search?.userId] = {
          ...collector.restore[search?.userId],
          ...data,
          userId: search?.userId,
        }
      }
      return collector.restore[search?.userId]
    },
    create: (data) => {
      let id = Object.values(collector.restore).length
      collector.restore[data.userId] = {
        id,
        ...data,
      }
      return collector.restore[data.userId]
    },
  },
}
const remainder = {
  transporter: {
    send: (...args) => collector.notifies.forEach(notify => notify(...args)),
  }
}

module.exports = (port = 5555) => {
  const app = express()

  // Включаем парсер параметров запроса
  app.use(express.json({ limit: '50mb' }))
  app.use(express.urlencoded({ limit: '50mb', extended: false }))

  // Подключаем тестируемую библиотеку
  app.use(authRouter(dbMethods, remainder))

  // маршрут, доступ к которому должен иметься у неавторизованного пользователя
  app.get('/unsecure', (req, res) => {
    res.send({ message: 'Если вы это видите, значит все хорошо' })
  })

  // маршрут, доступ к которому должен ОТСУТСВОВАТЬ у неавторизованного пользователя
  app.get('/secure', secure(), (req, res) => {
    if (req.user.id == 6) {
      return res.send({ message: 'Если вы это видите, значит все хорошо' })
    }
    res.send({ message: 'Если вы это видите, значит все ПЛОХО' })
  })

  // Запускаем прослушивание
  let server = app.listen(port)
  // console.log(`Тестовый сервер запущен, порт ${port}`);

  return {
    server,
    EMAIL,
    PASSWORD,
    onNotify: (fn) => {
      return collector.notifies.push(fn)
    }
  }
}
