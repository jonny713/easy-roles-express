
const startServer = require('./server')
const startTester = require('./tester')

const EMAIL = 'mail1@ya.ru'
const PASSWORD = '12345678'

describe('Easy-roles', () => {
  const PORT = 5555
  let EMAIL, PASSWORD, server, get, post, token, code;

  beforeAll(() => {
    ({ EMAIL, PASSWORD, server, onNotify } = startServer(PORT));
    ({ get, post } = startTester(PORT));
  })

  afterAll(() => {
    server.close();
  })

  test('Подключения модуля', async () => {
    let response = await get('/')

    expect(response.message).toBeTruthy();
  });

  test('Регистрации', async () => {
    let response = await post('/signup', { email: EMAIL, password: PASSWORD })

    expect(response.user).toBeTruthy();
  });

  test('Логин', async () => {
    let response = await post('/login', { email: EMAIL, password: PASSWORD })

    expect(response.token).toBeTruthy();

    token = response.token
  });

  test('Токен', async () => {
    // Передаем имя пользователя и пароль
    let response = await get('/user', null, { Authorization: token })

    expect(response?.user?.email).toBe(EMAIL);
  });

  test('Логаут', async () => {
    // Передаем имя пользователя и пароль
    let response = await post('/logout', null, { Authorization: token })

    expect(response.success).toBeTruthy();
  });

  test('Запрос общедоступного маршрута', async () => {
    // Передаем имя пользователя и пароль
    let response = await get('/unsecure')

    expect(response.message).toBeTruthy();
  });

  test('Запрос недоступного маршрута', async () => {
    // Передаем имя пользователя и пароль
    let response = await get('/secure')

    expect(response.code).toBe('not auth');
  });

  test('Сброс пароля. Отправка кода', async () => {
    // Вешаем слушатель кода
    onNotify(({ text }) => code = text.replace(/[^:]+:\s/, ''))

    // Передаем имя пользователя и пароль
    let response = await post('/restore/send-code', { email: EMAIL })

    expect(response.success).toBeTruthy();
  });

  test('Сброс пароля. Новый пароль', async () => {
    // Передаем имя пользователя и пароль
    let response = await post('/restore/password', { email: EMAIL, code, password: PASSWORD })

    expect(response.success).toBeTruthy();
  });
});
