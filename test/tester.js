const bent = require('bent')

let getBent
let postBent

const createTester = async (port) => {
  getBent = bent(`http://localhost:${port}`, 'GET', 'json', 200);
  postBent = bent(`http://localhost:${port}`, 'POST', 'json', 200);
}

const get = async (path, ...args) => {
  try {
    const response = await getBent(path, ...args)
    // console.log(`GET ${path}: `, response)

    return response
  } catch (e) {
    try {
      const json = await e.json()
      // console.error(`GET ${path}: ${e.statusCode}`, e.message, json)

      return json
    } catch (_e) {
      // console.error(`GET ${path}: ${e.statusCode}`, e.message)
    }
  }
}

const post = async (path, ...args) => {
  try {
    const response = await postBent(path, ...args)
    // console.log(`POST ${path}: `, response)

    return response
  } catch (e) {
    try {
      const json = await e.json()
      // console.error(`POST ${path}: ${e.statusCode}`, e.message, json)

      return json
    } catch (_e) {
      // console.error(`POST ${path}: ${e.statusCode}`, e.message)
    }
  }
}

module.exports = (port = 5555) => {
  createTester(port)

  return { get, post }
}
